#
# Provider Configuration
#

provider "aws" {
  region = "${var.region}"
}

# Using these data sources allows the configuration to be
# generic for any region.
data "aws_region" "current" {}

data "aws_availability_zones" "available" {
  state = "available"

  # This `aws_availability_zones` data source exports a `names` attribute that
  # is a list of Availability Zones names. This feature is further used in the
  # `vpc.tf` module in `aws_subnet` resource to dynamically create 2 subnets in
  # different availability zones.
}

# Not required: currently used in conjuction with using icanhazip.com to 
# determine local workstation external IP to open EC2 Security Group access to 
# the Kubernetes cluster.
# See workstation-external-ip.tf for additional information.
provider "http" {}
