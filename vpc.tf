#
# Virtual Private Cloud configuration.
#

# Create a 10.0.0.0/16 VPC.
resource "aws_vpc" "test" {
  cidr_block = "10.0.0.0/16"

  tags = "${
    map(
      "Name", "test-eks-node",
      "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}

# Create 2 subnets 10.0.x.0/24.
resource "aws_subnet" "test" {
  count = 2

  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  cidr_block        = "10.0.${count.index}.0/24"
  vpc_id            = "${aws_vpc.test.id}"

  tags = "${
    map(
      "Name", "test-eks-node",
      "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
}

# Create an internet gateway.
resource "aws_internet_gateway" "test" {
  vpc_id = "${aws_vpc.test.id}"

  tags = {
    Name = "test-eks-cluster"
  }
}

# Setup subnet routing external traffic through internet gateway.
resource "aws_route_table" "test" {
  vpc_id = "${aws_vpc.test.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.test.id}"
  }
}

resource "aws_route_table_association" "test" {
  count = 2

  subnet_id      = "${aws_subnet.test.*.id[count.index]}"
  route_table_id = "${aws_route_table.test.id}"
}
