#
# EKS Cluster resources
#

# IAM role.
resource "aws_iam_role" "test-cluster" {
  name = "${var.cluster-name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# Policy to allow EKS service to manage or retrieve data from other AWS 
# services.
resource "aws_iam_role_policy_attachment" "test-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.test-cluster.name}"
}

resource "aws_iam_role_policy_attachment" "test-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.test-cluster.name}"
}

# EKS Master Cluster security group.
resource "aws_security_group" "test-cluster" {
  name        = "${var.cluster-name}"
  description = "Cluster communication with worker nodes"
  vpc_id      = "${aws_vpc.test.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # Any protocol.
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.cluster-name}"
  }
}

# OPTIONAL: Allow inbound traffic from your local workstation external IP
# to the Kubernetes. You will need to replace A.B.C.D below with your real IP.
# resource "aws_security_group_rule" "test-cluster-ingress-workstation-https" {
#   cidr_blocks       = ["A.B.C.D/32"]
#   description       = "Allow workstation to communicate with the cluster API Server"
#   from_port         = 443
#   protocol          = "tcp"
#   security_group_id = "${aws_security_group.test-cluster.id}"
#   to_port           = 443
#   type              = "ingress"
# }

# Actual Kubernetes master cluster.
resource "aws_eks_cluster" "test" {
  name     = "${var.cluster-name}"
  role_arn = "${aws_iam_role.test-cluster.arn}"
  
  vpc_config {
    security_group_ids = ["${aws_security_group.test-cluster.id}"]
    subnet_ids         = "${aws_subnet.test.*.id}"
  }

  depends_on = [
    "aws_iam_role_policy_attachment.test-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.test-cluster-AmazonEKSServicePolicy",
  ]
}
