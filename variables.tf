#
# EKS Cluster infrastructure definition variables.
#
# Variables can also be set directly on the command line. E.g.:
# `terraform apply -var 'region=us-east-2'`
#
variable "region" {
  default = "us-east-1"
  type    = "string"
}

variable "cluster-name" {
  default = "test-eks-cluster"
  type    = "string"
}

variable "node-name" {
  default = "test-eks-node"
  type    = "string"
}
