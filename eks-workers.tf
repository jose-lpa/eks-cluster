#
# EKS Worker nodes
#

# IAM role.
resource "aws_iam_role" "test-node" {
  name = "${var.node-name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

# Policy to allow the worker nodes to manage or retrieve data from other AWS
# services. Used by Kubernetes to allow worker nodes to join the server.
resource "aws_iam_role_policy_attachment" "test-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.test-node.name}"
}

resource "aws_iam_role_policy_attachment" "test-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.test-node.name}"
}

resource "aws_iam_role_policy_attachment" "test-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.test-node.name}"
}

resource "aws_iam_instance_profile" "test-node" {
  name = "${var.cluster-name}"
  role = "${aws_iam_role.test-node.name}"
}

# Security group to control networking access to Kubernetes worker nodes.
resource "aws_security_group" "test-node" {
  name        = "${var.node-name}"
  description = "Security group for all nodes in the cluster"
  vpc_id      = "${aws_vpc.test.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1" # Any protocol.
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${
    map(
      "Name", "${var.node-name}",
      "kubernetes.io/cluster/${var.cluster-name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "test-node-ingress-self" {
  description              = "Allow nodes to communicate with each other"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "-1" # Any protocol
  security_group_id        = "${aws_security_group.test-node.id}"
  source_security_group_id = "${aws_security_group.test-node.id}"
  type                     = "ingress"
}

resource "aws_security_group_rule" "test-node-ingress-cluster" {
  description              = "Allow worker kubelets and pods to receive communication"
  from_port                = 1025
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test-node.id}"
  source_security_group_id = "${aws_security_group.test-node.id}"
  type                     = "ingress"
}

# Allow the worker nodes networking access to the EKS master cluster.
resource "aws_security_group_rule" "test-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with cluster API server"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.test-node.id}"
  source_security_group_id = "${aws_security_group.test-node.id}"
  type                     = "ingress"
}

# Worker node AutoScale configuration.
data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.test.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

# AutoScaling Launch Configuration that uses all our prerequisite resources to 
# define how to create EC2 instances using them.
# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We implement a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  test-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.test.endpoint}' --b64-cluster-ca '${aws_eks_cluster.test.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "test" {
  associate_public_ip_address = true
  iam_instance_profile        = "${aws_iam_instance_profile.test-node.name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  instance_type               = "m4.large"
  name_prefix                 = "${var.cluster-name}"
  security_groups             = ["${aws_security_group.test-node.id}"]
  user_data_base64            = "${base64encode(local.test-node-userdata)}"

  lifecycle {
    create_before_destroy = true
  }
}

# Create an AutoScaling Group that actually launches EC2 instances based on 
# previous onfiguration.
resource "aws_autoscaling_group" "test" {
  desired_capacity     = 2
  launch_configuration = "${aws_launch_configuration.test.id}"
  max_size             = 2
  min_size             = 1
  name                 = "${var.cluster-name}"
  vpc_zone_identifier  = "${aws_subnet.test.*.id}"

  tag {
    key                 = "Name"
    value               = "${var.cluster-name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
